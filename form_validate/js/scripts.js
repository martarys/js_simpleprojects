(function() {

var form = document.querySelector("#myForm"),
    fields = form.querySelectorAll("[data-error]");

function isNotEmpty(field){
    return field.value !== "";
    //zwróci true gdy ciąg nie jest pusty
}

function isAtLeast(field, min){
    return field.value.length >= min;
}

function isEmail(field){
    return field.value.indexOf("@") !== -1;
}

function displayErrors(errors){
    //sprawdzamy, czy jest element ul z klasa errors w pliku html:
    var ul = document.querySelector("ul.errors");

    if(!ul){
        ul = document.createElement("ul");

        ul.classList.add("errors");
    }

    //kasujemy to co jest w elemencie html ul - żeby kasowac błędy z listy na stronie, gdy coś już będzi epoprwnie
    ul.innerHTML = "";

    errors.forEach(function(error) {
        var li = document.createElement("li");

        li.textContent = error;

        ul.appendChild(li);

    });

    //chcemy wstawić liste błędów powyżej formularza na stronie:
    form.parentNode.insertBefore(ul, form);
}

form.addEventListener("submit", function(e){

    //zatrzymujemy domyślną akcję przeglądarki - nie wysyłamy form, bo chcemy go sprawdzić
    e.preventDefault();

    var errors = [];

    for(var i = 0; i < fields.length; i++) {

        var field = fields[i],
        isValid = false;

        if(field.type === "text"){
            isValid = isNotEmpty(field);
        } else if(field.type === "email"){
            isValid = isEmail(field);
        } else if(field.type === "select-one"){
            isValid = isNotEmpty(field);
        } else if(field.type === "textarea"){
            isValid = isAtLeast(field, 2);
        }
        
        if(!isValid){
            field.classList.add("error");
            errors.push(field.dataset.error);
        } else {
            field.classList.remove("error");
        }
    }

    //sprawdzamy, czy w ogóle sa jakieś błędy:
    if(errors.length){
        displayErrors(errors);
    } else {
        //metoda, która wyśle formularz:
        form.submit();
    }

    console.log(errors);

}, false);

    // # - pobieranie wg klasy, [] - pobieranie wg posiadania atrybutu




})();