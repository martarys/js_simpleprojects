(function() {

    var btn = document.querySelector("#getNumbers"),
        output = document.querySelector("#showNumbers");

    function getRandom(min, max) {
        return Math.round(Math.random() *(max-min) + min);
    }

//przypisujemy referencję do window, żeby było dostepne globalnie
    // window.getRandom = getRandom;


    function showRandomNumbers() {
        var numbers = [],
            num;


        for (var i = 0; i < 6; i++){
            num = getRandom(1, 49);

            while(numbers.indexOf(num) !== -1) {
                num = getRandom(1, 49);
                console.log("Powtórzyła się " + num);
            }
            numbers.push(num);
    
        }

        output.value = numbers.join(", ");

    }

    btn.onclick = showRandomNumbers;

})();