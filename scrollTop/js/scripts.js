(function() {


    function createButton(){
        var button = document.createElement("button");
    
        button.classList.add("backToTop", "hidden");
        button.textContent = "Powrót do góry";
        document.body.appendChild(button);
    
        return button;
    
    }
    
    
    var button = createButton();
    
    function animateScroll(){
        if(document.documentElement.scrollTop > 0){
            //przesuwamy stronę o 5px w górę
            window.scrollBy(0, -5);
            //wykonujemy przesunięcie o 5px co 10milisekund:
            setTimeout(animateScroll, 10);
        }
    
    }
    
    button.addEventListener("click", function(e){
    
        e.stopPropagation();
    
        // //przesuwamy na górę strony - sposób 1:
        // document.documentElement.scrollTop = 0;
    
        //przesuwamy na górę strony - sposób 2:
    
        animateScroll();
    
    }, false);
    
    window.addEventListener("scroll", function(e){
    
        // ///wyświetlimy ile px przeskrolowaliśmy:
        // console.log(document.documentElement.scrollTop);
    
        if(document.documentElement.scrollTop >= 100){
            button.classList.remove("hidden");
    
        } else {
            button.classList.add("hidden");
        }
    
    }), false
    
    
    
    })();