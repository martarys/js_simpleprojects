(function() {

var table = document.querySelector("#myTable"),
    ths = table.querySelectorAll("thead th"),
    trs = table.querySelectorAll("tbody tr");

//musimy zamienić ths na tablicę, żeby móc skorzystać z metody indexOf
//W standardzie ES6 dodano metodę Array.from, dzięki której można przekształcać obiekty tablicopodobne w tablicy. Czyli odnosząc się do tej lekcji napisalibyśmy var ths = Array.from(table.querySelectorAll("thead th"));

function makeArray(nodeList) {
    var arr = [];

    for(var i = 0; i < nodeList.length; i++){
        arr.push(nodeList[i]);
    }
    return arr;
} 

function clearClassName(nodeList) {
    
    for(var i = 0; i < nodeList.length; i++){

        nodeList[i].className = "";    
    }

}

function sortBy(e) {

    var target = e.target,
        thsArr = makeArray(ths),
        //wiersze też chcemy zamienić na tablicę:
        trsArr = makeArray(trs),
        index = thsArr.indexOf(target),
        df = document.createDocumentFragment(),
        order = (target.className === "" || target.className === "desc") ? "asc" : "desc";

    clearClassName(ths);

 

//kiedy trsArr jest tblica - możemy sort():
    trsArr.sort(function(a, b){

        var tdA = a.children[index].textContent,
            tdB = b.children[index].textContent;

        if(tdA < tdB) {
            return order === "asc" ? -1 :1;            
        } else if(tdA > tdB){
            return order === "asc" ? 1:-1;
        } else {
            return 0;
        }
    });

    trsArr.forEach(function(tr){
        df.appendChild(tr);
    });

    target.className = order;
    //dodajemy element, który ju z istniej ena stronie, więc przeglądarka wie, że trzeba tylk,o zamienić kolejność
    table.querySelector("tbody").appendChild(df);
}

//przypisujemy zdarzenie do wielu elementów przez pętlę:
for(var i = 0; i < ths.length; i++){

    ths[i].onclick = sortBy;
}

})();