(function(){


    var globalTooltip = null;


    function assignEvents(elems, type, event){

        for(var i = 0; i < elems.length; i++){

            elems[i].addEventListener(type, event, false);
        }
    }

    function showTooltip(e){

        var options = {
            w: e.target.offsetWidth,
            x: e.target.offsetLeft,
            y: e.target.offsetTop
        };



        var text = e.target.getAttribute("title");

        createTooltip(text, options);

        e.target.removeAttribute("title");
    }

    function createTooltip(text, options){

        var toolTip = document.createElement("div");

        toolTip.className = "tooltip hidden";
        toolTip.appendChild(document.createTextNode(text));
        
        document.body.appendChild(toolTip);

        toolTip.style.top = options.y - toolTip.offsetHeight - 10 + "px";
        toolTip.style.left = options.x + options.w/2 - toolTip.offsetWidth/2 + "px";

        toolTip.classList.remove("hidden");



        globalTooltip = toolTip;

    }



    function removeTooltip(e){

        e.target.setAttribute("title", globalTooltip.textContent);
        globalTooltip.parentNode.removeChild(globalTooltip);
    }

    function init(elems){

       assignEvents(elems, "mouseenter", showTooltip);
       assignEvents(elems, "mouseleave", removeTooltip);

    }
    
    //eksportujemy referencję do init,
    window.t00ltip = init;
    
})();
    